<?php

/*
 *  @file clouseau-server.class.php
 *  @author Robin Millette <robin@millette.info>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Client Configuration
define("CONFIG_FILE_PATH", "./");
define("CONFIG_FILE_NAME", "clientconf.xml");
$clousrvaddr = 'http://localhost/~mathieu/clou/clousrv.php';
$dir = dir('/proc');
$pkgs = array();
$n = 2;
$path_to_file = CONFIG_FILE_PATH . CONFIG_FILE_NAME;

function generate_clouid () {
	uuid_create(&$uuid);
	uuid_make($uuid, UUID_MAKE_V4);
	uuid_export($uuid, UUID_FMT_STR, &$uuidstring);
	trim($uuidstring);
	
	return $uuidstring;
}

if (file_exists($path_to_file)) {
	$config_file = simplexml_load_file($path_to_file);

	//print_r($config_file);
	if (!empty($config_file->clouid)) {
		echo "clouid already set: " . $config_file->clouid . "\n\n";
	} else {
		echo "Generating new clouid for host... ";
		$config_file->clouid = generate_clouid();
		//print_r($config_file);
		file_put_contents($path_to_file, $config_file->asXML()); 
	}
} else {
	exit('Failed to open configuration xml file.');
}

// Find the licence associated with a process by feeding its cmdline to dpkg-query
while (false !== ($procdir = $dir->read())) {
	$pid = intval($procdir);
	if (!$pid) continue;

	$cmdline = file_get_contents("/proc/$pid/cmdline");
	if (!$cmdline || ('/' !== $cmdline{0})) continue;

	list($exec) = explode(chr(0), $cmdline, 2);
	if (isset($pkgs[$exec])) continue;

	if (!--$n) break;
	
	list($pkg) = explode(':', exec("dpkg-query --search $exec"), 2);
	$pkgs[$exec] = array(
		  'pkg' => $pkg
		, 'licence' => "/usr/share/doc/$pkg/copyright");
}

$dir->close();

// Create POST context
$opts = array(
	'http' => array(
		  'method' => 'POST'
		, 'content' => http_build_query($pkgs)
		, 'header'  => "Content-type: application/x-www-form-urlencoded\r\n"
	)
);

$context = stream_context_create($opts);

// Push the inspection report ($pkgs) to the Clouseau server
$reply = file_get_contents($clousrvaddr, false, $context);

echo "After a successfull POST, the client received the following reply from the server:\n\n $reply\n\n";

?>
