<?php

/*
 *  @file clouseau-client.class.php
 *  @authors Robin Millette <robin@millette.info>
 *    et 
 *  Mathieu Gauthier-Pilote <mathieu.g.p@republiquelibre.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Clouseau Client Class. The clouseau client generates a report on the
 * licences associated with the packages installed and running on the 
 * localhost.
 *
 */

class ClouseauClient {

	var $client_config = array();
	var $local_report = array();
	var $path_to_config_file;

	// Instanciate a clouseau object
	function ClouseauClient($path_to_file) {
		if (file_exists($path_to_file)) {
			$this->client_config = simplexml_load_file($path_to_file);
			$this->path_to_config_file = $path_to_file;
		}
		else {
			echo('Failed to open config file.');
		}
	}
	
	function generate_clouid () {
		uuid_create(&$uuid);
		uuid_make($uuid, UUID_MAKE_V4);
		uuid_export($uuid, UUID_FMT_STR, &$uuidstring);
		trim($uuidstring);
	
		return $uuidstring;
	}

	// Print client configuration to screen
	function print_config() {
		print_r($this->client_config);
	}

	// Generate the licence report
	function generate_report($limit) {
		$dir = dir('/proc');
		
		while (false !== ($procdir = $dir->read())) {
			$pid = intval($procdir);
			if (!$pid) continue;

			$cmdline = file_get_contents("/proc/$pid/cmdline");
			if (!$cmdline || ('/' !== $cmdline{0})) continue;

			list($exec) = explode(chr(0), $cmdline, 2);
			if (isset($this->local_report[$exec])) continue;

			if (isset($limit) && !--$limit) break;
	
			list($pkg) = explode(':', exec("dpkg-query --search $exec"), 2);
			$this->local_report['pkgs'][$exec] = array(
				  'pkg' => $pkg
				, 'licence' => "/usr/share/doc/$pkg/copyright");
		}

		$dir->close();
	}
	
	// Send report to a clouseau server via POST
	function send_report() {
		
		// First check if there is something to send...
		
		
		// Get a clouid from config file or else generate one
		//print_r($this->client_config);
		if (!empty($this->client_config->clouid)) {
			echo "clouid already set: " . $this->client_config->clouid  . "\n\n";
		} else {
			echo "Generating new clouid for host... ";
			$this->client_config->clouid = $this->generate_clouid();
			//print_r($this->client_config);
			file_put_contents($this->path_to_config_file, $this->client_config->asXML()); 
		}
		
		$this->local_report['clouid'] = $this->client_config->clouid;
		
		// Create POST context
		$opts = array(
			'http' => array(
			  'method' => 'POST'
			, 'content' => http_build_query($this->local_report)
			, 'header'  => "Content-type: application/x-www-form-urlencoded\r\n"
			)
		);

		$context = stream_context_create($opts);

		// Push the inspection report to the Clouseau server
		$reply = file_get_contents($this->client_config->clousrv_addr, false, $context);

		echo "After a successfull POST, the client received the following reply from the server:\n\n $reply\n\n";
	}
	
}

?>
