<?php

/*
 *  @file cloucli.php
 *  @author Mathieu Gauthier-Pilote <mathieu.g.p@republiquelibre.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Configure custom PHP settings
error_reporting(1); // report all errors
ini_set('display_errors', 1); // display all errors

require 'clouseau-client.class.php';

// Client Configuration
define("CONFIG_FILE_PATH", "./");
define("CONFIG_FILE_NAME", "clientconf.xml");
$path_to_file = CONFIG_FILE_PATH . CONFIG_FILE_NAME;

// Create clouseau client object
$cloucli = new ClouseauClient($path_to_file);
$cloucli->print_config();
$cloucli->generate_report(3);
$cloucli->send_report();

?>
