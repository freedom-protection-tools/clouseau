<?php

/*
 *  @file clouseau-server.class.php
 *  @author Mathieu Gauthier-Pilote <mathieu.g.p@republiquelibre.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Clouseau Server Class. Clouseau server reads a config file
 * from which it gets an inspection list and the optional fields it
 * needs to generate a global licence report.
 *
 */

class ClouseauServer {

	var $server_config = array();

	// Instanciate a clouseau object
	function ClouseauServer($path_to_file) {
		if (file_exists($path_to_file)) {
			$this->server_config = simplexml_load_file($path_to_file);
		}
		else {
			echo('Failed to open config file.');
		}
	}

	// Print server configuration to screen
	function print_config() {
		//print_r($this->server_config);
	}

	// Get incoming requests from clouseau clients
	function get_inspection_reports() {
		$allowed_addresses = explode(",", $this->server_config->inspection_list_by_ip);
		$remote_address = $_SERVER['REMOTE_ADDR'];
		$pkgs = $_POST['pkgs'];
		$clouid = $_POST['clouid'][0];
		//print_r($pkgs);
		//print_r($clouid);

		if (in_array($remote_address, $allowed_addresses)) {
			try {
				$dbh = new PDO('sqlite:clouseau.db');
				//$dbh->exec("CREATE TABLE inspections (clouid TEXT PRIMARY KEY, report TEXT)");
				
				$report = $dbh->quote(serialize($pkgs));
				$sql = "SELECT * FROM inspections WHERE clouid = '$clouid'";
				$result = $dbh->query($sql);
				if ($result->fetch(PDO::FETCH_ASSOC) ) {
					echo "Host record was found: updating inspection result based on new POST...\n\n";
					$dbh->exec("UPDATE inspections SET report=$report WHERE clouid='$clouid'");
				}
				else {
					echo "No record found for incoming POST from remote host. Creating new record...\n\n";
					$dbh->exec("INSERT INTO inspections (clouid, report) VALUES ('$clouid', $report)");
				}
				
				foreach ($dbh->query('SELECT * FROM inspections', PDO::FETCH_ASSOC) as $row) { 
					print_r($row);
				}
			
				$dbh = NULL;
			}
			catch(PDOException $e)	{
				print 'Exception : '.$e->getMessage();
			}
		}
	}

	// Merge all data for the final report
	function generate_global_report() {
		$dbh = new PDO('sqlite:clouseau.db');
		foreach ($dbh->query('SELECT * FROM inspections', PDO::FETCH_ASSOC) as $row) { 
			print_r($row); 
		}
	}
}

?>
